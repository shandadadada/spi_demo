package com.guolin.test.adaptive;
import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.extension.Adaptive;
import org.apache.dubbo.common.extension.SPI;

@SPI("dubbo")
public interface RpcAdaptiveExt {

    @Adaptive({"p"})
    String echo(String msg, URL url);
}
