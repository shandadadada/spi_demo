package com.guolin.test.adaptive.impl;

import org.apache.dubbo.common.URL;
import com.guolin.test.adaptive.RpcAdaptiveExt;
import org.apache.dubbo.common.extension.Adaptive;

/**
 * @ClassName : ThriftAdaptiveExt
 * @Description : thrift
 * @Author : xueguolin
 * @Date: 2020-11-02 20:23
 */
//@Adaptive
public class ThriftAdaptiveExt implements RpcAdaptiveExt {
    @Override
    public String echo(String msg, URL url) {
        return "thrift";
    }
}
