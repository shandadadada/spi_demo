package com.guolin.test.adaptive.impl;

import com.guolin.test.adaptive.RpcAdaptiveExt;
import org.apache.dubbo.common.URL;

/**
 * @ClassName : DubboAdaptiveExt
 * @Description :
 * @Author : xueguolin
 * @Date: 2020-11-02 20:22
 */
public class DubboAdaptiveExt implements RpcAdaptiveExt {
    @Override
    public String echo(String msg, URL url) {
        return "dubbo";
    }
}
