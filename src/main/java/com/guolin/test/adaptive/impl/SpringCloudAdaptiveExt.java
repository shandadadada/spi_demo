package com.guolin.test.adaptive.impl;

import org.apache.dubbo.common.URL;
import com.guolin.test.adaptive.RpcAdaptiveExt;

/**
 * @ClassName : SpringCloudAdaptiveExt
 * @Description : spring cloud
 * @Author : xueguolin
 * @Date: 2020-11-02 20:23
 */
public class SpringCloudAdaptiveExt implements RpcAdaptiveExt {
    @Override
    public String echo(String msg, URL url) {
        return "spring cloud";
    }
}
