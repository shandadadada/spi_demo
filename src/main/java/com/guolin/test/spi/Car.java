package com.guolin.test.spi;


import org.apache.dubbo.common.extension.SPI;

@SPI
public interface Car {
    String carName();
}
