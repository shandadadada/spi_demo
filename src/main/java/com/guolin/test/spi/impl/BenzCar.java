package com.guolin.test.spi.impl;

import com.guolin.test.spi.Car;

/**
 * @ClassName : Benz
 * @Description : 奔驰
 * @Author : xueguolin
 * @Date: 2020-10-31 14:52
 */
public class BenzCar implements Car {

    @Override
    public String carName() {
        return "奔驰";
    }
}

