package com.guolin.test.spi.impl;

import com.guolin.test.spi.Car;

/**
 * @ClassName : BMWCar
 * @Description : 宝马
 * @Author : xueguolin
 * @Date: 2020-10-31 14:50
 */
public class BMWCar implements Car {
    @Override
    public String carName() {
        return "BMW";
    }
}
