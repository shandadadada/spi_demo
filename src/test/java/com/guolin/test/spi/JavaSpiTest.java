package com.guolin.test.spi;


import com.sun.tools.javac.util.ServiceLoader;
import org.junit.Test;
import sun.misc.Service;

import java.util.Iterator;

/**
 * @ClassName : JavaSpiTest
 * @Description : spi测试
 * @Author : xueguolin
 * @Date: 2020-10-31 14:54
 */
public class JavaSpiTest {

    @Test
    public void testJavaSpi() throws Exception {

        // 方式1：通过 Service.providers 获取实现类的实例
        Iterator<Car> providers = Service.providers(Car.class);
        System.out.println("Java SPI Service.providers :");
        while (providers.hasNext()) {
            Car car = providers.next();
            System.out.println(car.carName());
        }

        // 方式2：通过 ServiceLoader.load 获取
        ServiceLoader<Car> load = ServiceLoader.load(Car.class);
        System.out.println("Java SPI ServiceLoader.load :");
        Iterator<Car> iterator = load.iterator();
        while (iterator.hasNext()) {
            Car car = iterator.next();
            System.out.println(car.carName());
        }
    }
}
