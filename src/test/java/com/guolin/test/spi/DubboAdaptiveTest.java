package com.guolin.test.spi;

import com.guolin.test.adaptive.RpcAdaptiveExt;
import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.extension.ExtensionLoader;
import org.junit.Test;

/**
 * @ClassName : DubboAdaptiveTest
 * @Description :
 * @Author : xueguolin
 * @Date: 2020-11-02 20:25
 */
public class DubboAdaptiveTest {
    /**
     * AdaptiveExt SPI注解 value 为空，通过url的test.adaptive.ext参数传值
     * url的key=rpc.adaptive.ext（类名单词小写拆分）
     * 输出：rpc.adaptive.ext的值
     */
    @Test
    public void test1() {
        ExtensionLoader<RpcAdaptiveExt> loader = ExtensionLoader.getExtensionLoader(RpcAdaptiveExt.class);
        RpcAdaptiveExt adaptiveExtension = loader.getAdaptiveExtension();
        URL url = URL.valueOf("test://localhost/test?rpc.adaptive.ext=dubbo");
        System.out.println(adaptiveExtension.echo("d", url));
    }

    /**
     * SPI注解中有value值@SPI("dubbo")
     * 输出：@SPI的值
     */
    @Test
    public void test2() {
        ExtensionLoader<RpcAdaptiveExt> loader = ExtensionLoader.getExtensionLoader(RpcAdaptiveExt.class);
        RpcAdaptiveExt adaptiveExtension = loader.getAdaptiveExtension();
        URL url = URL.valueOf("test://localhost/test");
        System.out.println(adaptiveExtension.echo("d", url));
    }

    /**
     * SPI注解中有value值@SPI("dubbo")，URL中也有具体的值
     * 输出：@SPI的值
     */
    @Test
    public void test3() {
        ExtensionLoader<RpcAdaptiveExt> loader = ExtensionLoader.getExtensionLoader(RpcAdaptiveExt.class);
        RpcAdaptiveExt adaptiveExtension = loader.getAdaptiveExtension();
        URL url = URL.valueOf("test://localhost/test?rpc.adaptive.ext=cloud");
        System.out.println(adaptiveExtension.echo("d", url));
    }

    /**
     * SPI注解中有value值@SPI("dubbo")，URL中也有具体的值，实现类上面有
     * 输出@Adaptive的值
     */
    @Test
    public void test4() {
        ExtensionLoader<RpcAdaptiveExt> loader = ExtensionLoader.getExtensionLoader(RpcAdaptiveExt.class);
        RpcAdaptiveExt adaptiveExtension = loader.getAdaptiveExtension();
        URL url = URL.valueOf("test://localhost/test?rpc.adaptive.ext=cloud");
        System.out.println(adaptiveExtension.echo("d", url));
    }

    /**
     * SPI注解中有value值@SPI("dubbo")，URL中也有具体的值，注解中的value与链接中的参数的key一致, 实现类上面没有@Adaptive
     * 输出：参数的key的值
     */
    @Test
    public void test5() {
        ExtensionLoader<RpcAdaptiveExt> loader = ExtensionLoader.getExtensionLoader(RpcAdaptiveExt.class);
        RpcAdaptiveExt adaptiveExtension = loader.getAdaptiveExtension();
        URL url = URL.valueOf("test://localhost/test?p=cloud");
        System.out.println(adaptiveExtension.echo("d", url));
    }
}
