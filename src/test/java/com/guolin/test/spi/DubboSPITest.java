package com.guolin.test.spi;

import org.apache.dubbo.common.extension.ExtensionLoader;
import org.junit.Test;

/**
 * @ClassName : DubboSPITest
 * @Description : dubboSpi测试
 * @Author : xueguolin
 * @Date: 2020-10-31 15:16
 */
public class DubboSPITest {
    @Test
    public void test() throws Exception {
        ExtensionLoader<Car> extensionLoader = ExtensionLoader.getExtensionLoader(Car.class);
        System.out.println("DUBBO SPI :");
        Car bmw = extensionLoader.getExtension("bmwCar");
        System.out.println(bmw.carName());
        Car benz = extensionLoader.getExtension("benzCar");
        System.out.println(benz.carName());
    }
}
