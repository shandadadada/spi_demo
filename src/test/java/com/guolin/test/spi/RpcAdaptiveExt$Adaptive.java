package com.guolin.test.spi;

//package com.guolin.test.adaptive;

import org.apache.dubbo.common.extension.ExtensionLoader;


/**
 * @ClassName : RpcAdaptiveExt$Adaptive
 * @Description : @Adaptive生成的包装类
 * @Author : xueguolin
 * @Date: 2020-11-03 14:20
 */
public class RpcAdaptiveExt$Adaptive implements com.guolin.test.adaptive.RpcAdaptiveExt {
    public java.lang.String echo(java.lang.String arg0,
                                 org.apache.dubbo.common.URL arg1) {
        if (arg1 == null) {
            throw new IllegalArgumentException("url == null");
        }

        org.apache.dubbo.common.URL url = arg1;
        String extName = url.getParameter("p", "dubbo");

        if (extName == null) {
            throw new IllegalStateException(
                    "Failed to get extension (com.guolin.test.adaptive.RpcAdaptiveExt) name from url (" +
                            url.toString() + ") use keys([p])");
        }

        com.guolin.test.adaptive.RpcAdaptiveExt extension = (com.guolin.test.adaptive.RpcAdaptiveExt) ExtensionLoader.getExtensionLoader(com.guolin.test.adaptive.RpcAdaptiveExt.class)
                .getExtension(extName);

        return extension.echo(arg0, arg1);
    }
}
